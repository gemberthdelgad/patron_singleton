﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_singelton
{
    class Singleton
    {
        // aqui guardamos la unica instancia que va a existir
        private static Singleton instancia =null;

        //datos propios de la clase
        private string nombre;
        private int edad;
        //creamos el constructor 
        public Singleton()
        {
            nombre = "sin asignar";
            edad = 92;
        }



        //creamos el metodo estatico, que va a crear una sola instancia 
        public static Singleton oBtenerIntancia()
        {
            //verificamos si no existe la instancia
            if (instancia == null)
            // si no existe la instancia se crea 
            {
                instancia = new Singleton();
             
               
                Console.WriteLine("instancia creada por primera vez");
            }
            //regresamos instancia
            return instancia;
        }
        //aqui metodos propios de la clase
        public override string ToString()
        {
            return string.Format("la persona {0}, tiene edad {1},",nombre,edad);
        
        }

        public void PonerDatos(string pNombre,int pEdad)
        {
            nombre = pNombre;
            edad = pEdad;
        }
        //esto representa cualquier otro objeto
        public void ProcesoDeClase()
        {
            Console.WriteLine("{0} esta trabajando en algo",nombre);
            //return string.Format("la persona {0}, tiene edad {1},", nombre, edad);

        }





    }
}
