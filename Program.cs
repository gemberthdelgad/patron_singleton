﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_singelton
{
    class Program
    {
        static void Main(string[] args)
        {
            //la intancia que se crea por primera vez

            Singleton uno = Singleton.oBtenerIntancia();
            //hacemoso algo con la instancia
            uno.PonerDatos("Stiven",20);
            uno.ProcesoDeClase();
            Console.WriteLine(uno);
            Console.WriteLine("------------------");
            //obtenemos la instancia
            Singleton dos = Singleton.oBtenerIntancia();
            //comprovamos que es la misma instancia 
            //Como es la misma instancia tendra en mismo estado que hemos ingresado arriba
            Console.WriteLine(dos);
            Console.ReadKey();


        }
    }
}
